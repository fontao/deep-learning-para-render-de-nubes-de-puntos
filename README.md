# Una aproximación Deep Learning para render de nubes de puntos

Una nube de puntos es un conjunto de puntos de datos en el espacio. Las nubes de puntos
generalmente son producidas por escáneres 3D, que miden una gran cantidad de puntos en las
superficies externas de los objetos que los rodean. Como la salida del escaneo 3D procesos.
Las nubes de puntos se utilizan para muchos propósitos, incluso para crear modelos CAD en 3D,
para piezas manufacturadas, para metrología e inspección de calidad, y para una multitud de
visualización, animación, renderizado y aplicaciones de personalización en masa.

Para visualizar estas cantidades masivas de datos, uno necesita aprovechar las técnicas de
Renderizado Basado en Puntos (PBR).

La representación es el proceso de generar una imagen de un modelo mediante un programa de
software. El modelo es una descripción de objetos tridimensionales en una estructura de datos
estrictamente definida. Contiene geometría, punto de vista, textura e información de iluminación.
La representación es uno de los principales campos de los gráficos 3D por ordenador.

Usando puntos como la representación primitiva, las imágenes de salida se construyen a partir de
una nube de puntos y esto se conoce como renderizado basado en puntos (PBR).
Comparar mallas poligonales con puntos es análogo a comparar vectores gráficos con gráficos de
píxeles. Los puntos en 3D son análogos a los píxeles en 2D, reemplazando triángulos con textura o
más ordenar superficies por elementos de dimensión cero.
Dado que los puntos son adimensionales, deben usarse para calcular las normales y usarlas para
proporcionar la representación visual en una superficie, que luego se llama Splat.
El proceso de estimar las normales no es trivial.
Los conjuntos de puntos suelen ser bastante ruidosos y no tienen más información que una posición
de punto (x, y, z).

Las arquitecturas de aprendizaje profundo (Deep Learning) como redes neuronales profundas
(Deep Neural Networks) , redes de creencias profundas (Deep Belief Networks) y redes neuronales
recurrentes (Recurrent Neural Networks) han sido aplicadas a campos que incluyen visión mediante
ordenador, reconocimiento de voz, procesamiento de lenguaje natural, reconocimiento de audio,
filtrado de redes sociales, traducción automática, bioinformática, diseño de medicamentos,
análisis médico de imágenes, inspección de materiales y programas de juegos de mesa, donde han
producido resultados comparables o incluso superiores a los expertos humanos.

Los modelos de aprendizaje profundo están vagamente inspirados por el procesamiento de
información y los patrones de comunicación en sistemas nerviosos biológicos aún teniendo varias
diferencias con las propiedades estructurales y funcionales de los cerebros biológicos.
(especialmente cerebros humanos), que los hacen incompatibles con las evidencias de neurociencia.

El objetivo de este proyecto será la implementación de técnicas PBR de última generación y la
estimación normal de algoritmos.
Luego se probarán las técnicas de aprendizaje profundo para tratar de mejorar estos enfoques
clásicos. Para lograr estos resultados, OpenGL se utilizará para representar dichas primitivas en
tiempo real. En el aprendizaje profundo, por otro lado, se utilizará TensorFlow ya que es el estándar
de facto para los modelos de Aprendizaje automático y Aprendizaje profundo.

Los dominios de aplicación específicos son modelado 3D, ingeniería civil, arquitectura, etc.