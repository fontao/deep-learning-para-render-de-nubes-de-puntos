#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 01:00:53 2020

@author: msf
"""
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Activation, Flatten, Dense
from matplotlib import pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger, ReduceLROnPlateau

def load_inputs():
    print("\nWelcome to Model Trainer")
    predictorsPath = str(input(" -->Write the path to Predictors: " ))
    
    if predictorsPath:
        targetPath = str(input(" -->Write the path to Targets: " ))
    
    name = str(input(" -->Write the name for your model: "))

   
    if not predictorsPath or not targetPath:
        return
        #X = np.load('/home/msanchez/dev/INPUTS/3-NormalizedpredictorsSinResta.npy')
        #Y = np.load('/home/msanchez/dev/INPUTS/3-NormalizedtargetSinResta.npy')

        #X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=41)
        
        #if not name:
            #name = "trained_modelDEFAULT"
        #print("\nEmpty Predictors or Targets, so, loading DEFAULT inputs ...\n")
        #return X_train, X_test, y_train, y_test, name

    if not name:
        name="trained_model";

    X = np.load(predictorsPath)
    Y = np.load(targetPath)

    print("Loading your Predictors and Targets ...")

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=41)
    return X_train, X_test, y_train, y_test, name
   


def create_model(n_cols, init_mode='glorot_normal', opt='adam', loss='mse', metrics=[]):
    model =  Sequential()
    # Input
    model.add(Dense(128, kernel_initializer=init_mode, activation='relu', input_shape=(n_cols,)))
    model.add(Dense(64, kernel_initializer=init_mode, activation='relu'))
    model.add(Dense(32, kernel_initializer=init_mode, activation='relu'))
    # Output
    model.add(Dense(3, kernel_initializer=init_mode))
   
    model.compile(loss=loss,
                  optimizer=opt, metrics=metrics)
    # model.summary()
    return model


def get_lr_metric(optimizer):
    def lr(y_true, y_pred):
        return optimizer._decayed_lr(tf.float32) # I use ._decayed_lr method instead of .lr
    return lr


## Tunning PARAMETERS
# OPTIMIZADOR
#                      DEFAULT 0.001
def fit_model(X_train, y_train, X_test, y_test, name):

    n_epochs = str(input(" -->Select a number of Epochs (DEFAULT 500): " ))
    
    if not n_epochs:
        n_epochs = 500
    else:
        n_epochs = int(n_epochs)
        #if n_epochs < 1:
            #n_epochs = 2
        

    lrate = str(input(" -->Select a Learning Rate between 0.1 and 0.00001 (DEFAULT 0.001): "))

    if not lrate or lrate < 0.1 or lrate > 0.00001:
        lrate = 0.001
        
    loss_function = str(input(" -->Choose a loss function (DEFAULT MSE): "))
    print("\nALL SET!, Starting training ...\n")
    
    if not loss_function:
        loss_function = "mse"

   
    n_cols = X_train.shape[1]

    opt = keras.optimizers.Adam(learning_rate=lrate)
   
    lr_metric = get_lr_metric(opt)
   
    metrics = [lr_metric]
   
    model=create_model(n_cols, opt=opt, loss=loss_function, metrics=metrics)
   
    # model.compile(loss='mse', optimizer='rmsprop', metrics=['mae'])
   
    # Add callbacks
    # filepath = "saved_models/3-log3capasADAMMAE5ke-weights-improvement-{epoch:02d}-{val_loss:.2f}.hdf5"
    # checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
   
    # early_stop = EarlyStopping(monitor='val_loss', patience=30, verbose=1)
   
    log_csv = CSVLogger('../../Output/Model/logs/' + name + '.csv')
    reduceLR = ReduceLROnPlateau(monitor='loss', factor=0.5, patience=150)
   
    callbacks_list = [log_csv, reduceLR]
    # callbacks_list = [checkpoint, early_stop, log_csv]


    history = model.fit(X_train, y_train, verbose=1, batch_size=128, epochs=n_epochs,
                        validation_data=(X_test, y_test), callbacks=callbacks_list)
   
    model.save('../../Output/Model/saved_models/' + name + '.h5')
   
    return history

def plot(history, name):
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(1, len(loss) + 1)
    fig = plt.figure(figsize=(6, 3), dpi=200)
    plt.plot(epochs, loss, 'y', label='Training loss')
    plt.plot(epochs, val_loss, 'r', label='Validation loss')
    plt.title('Training and validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()
    fig.savefig('../../Output/Model/plots/' + name + '.png', bbox_inches='tight')

def main():
    X_train, X_test, y_train, y_test, name = load_inputs()
    history = fit_model(X_train, y_train, X_test, y_test, name)
    plot(history, name)

main() 