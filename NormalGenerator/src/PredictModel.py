#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 11:37:12 2021

@author: msanchez
"""
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Activation, Flatten, Dense
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from pyntcloud import PyntCloud
import os
from InputGenerator.InputGenerator import loadPC, splitPC, neighbors, standarizeOutput, assembleInput
import time


def __pointCloudToFile(predictedNormals, filepath):
    points = pd.DataFrame(predictedNormals, columns=['x', 'y', 'z'])

    cloud = PyntCloud(points)
    cloud.to_file('../Output/' + filepath)
 

def getFilePath():
    # /home/msanchez/dev/INPUTS/dollPred.npy
    # /home/msanchez/dev/INPUTS/dollTarg.npy
    # input
    filePath = str(input("Write the path to PLY file:" ))
    if not filePath:
        filePath = "../../IPointv1/src/PCs/doll.ply"
    fileName = os.path.basename(os.path.normpath(filePath))
    name = fileName[:-4]
    return filePath, name

def predictNormals(filePath):
    # Recrea exactamente el mismo modelo solo desde el archivo
    new_model = keras.models.load_model('./Model/DEFAULTPredictorModel.h5', compile = False)
    
    inputData, realNormals = assembleInput(filePath)
    start_time = time.time()
    predictedNormals = new_model.predict(inputData)
    print("Predict model execution time:  %s seconds" % (time.time() - start_time))
    return predictedNormals, realNormals

def generatePLYFile(predictedNormals, name):
    __pointCloudToFile(predictedNormals, 'predicted' + name + '.ply')

def calculateError(predictedNormals, realNormals):
    porcentaxeErro = ((predictedNormals - realNormals)/2)*100
    
    media = abs(np.sum(porcentaxeErro, axis=1)/3)
    
    puntos = np.arange(0.0, len(predictedNormals), 1.0)
    
    return puntos, media

#POR IMPLEMENTAR.
def calculateAngleError(predictedNormals, realNormals):
    ## Implementar diferencia de ángulos entre a normal realNormals, y predictedNormals.
    print(realNormals[0][0], predictedNormals[0][0])
    print(realNormals[0][1], predictedNormals[0][1])
    print(realNormals[0][2], predictedNormals[0][2])
    print(realNormals[0]*predictedNormals[0])
    print(realNormals[0][0] * predictedNormals[0][0])
    print(realNormals[0][1] * predictedNormals[0][1])
    print(realNormals[0][2] * predictedNormals[0][2])

def plot(puntos, media, name):
    fig = plt.figure(figsize=(10, 4), dpi=500)
    plt.subplot(221)
    plt.plot(puntos[0:500], media[0:500])
    plt.subplot(222)
    plt.plot(puntos[25000:25500], media[25000:25500])
    plt.subplot(223)
    plt.plot(puntos[300000:300500], media[300000:300500])
    plt.subplot(224)
    plt.plot(puntos[900300:900800], media[900300:900800])
    plt.suptitle('Normal Estimation Comparative')
    #plt.show()
    fig.savefig('../Output/Plots/predicted'+name+'.png', bbox_inches='tight')

def main():
    filePath, name = getFilePath()
    predictedNormals, realNormals = predictNormals(filePath)
    generatePLYFile(predictedNormals, name)
    puntos, media = calculateError(predictedNormals, realNormals)
    plot(puntos, media, name)
    print("ALL DONE!")
    


main()
