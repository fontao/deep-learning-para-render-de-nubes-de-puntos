#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 21:20:17 2021

@author: msanchez
"""

## TENDRA QUE HACER LA OPCION DE LOADPC DESDE UN DIRECTORIO

### Esta función permite añadir PointClouds a una lista, devuelve la lista con un PointCloud más añadido ###
## Requiere:
from plyfile import PlyData, PlyElement
def loadPC(my_list = [], filePath = ""):
    # Comprobamos que filePath contiene algo
    if filePath == "":
        print("The parameter filePath MUST contain a path to the PLY file")
        return
    # Load PointCloud, es necesario tener cargada .
    try:
        plydata = PlyData.read(filePath)
    except  OSError as e:
        print("Error",e.errno,": File \"",filePath,"\" not found")
        return
    # Add a new PointCloud to the list.
    my_list.append(plydata)
    return # Si añado aquí "return my_list" podría hacer unaa copia de la lista que meto por parámetro.


## A LA FUNCIÓN SPLITPC HAY QUE CAMBIAR LA MANERA DE NORMALIZAR LOS PUNTOS.

## Esta función recoge una lista llena de Nubes de puntos, y devuelve en dos listas separadas, 
## un array con todos los puntos XYZ y otro con sus normales.
## REQUIERE: 
def splitPC(my_list):
	if len(my_list)==0:
		print("The list passed cannot be empty")
		return
	newPoints = []
	newNormals = []
	for i in range(0, len(my_list)):
		## Así obtendremos un array con los datos XYZ
		points = []
		normals = []
		for j in range(0, my_list[i].elements[0].count):
			aux = []
			aux.append(my_list[i].elements[0].data[j][0])
			aux.append(my_list[i].elements[0].data[j][1])
			aux.append(my_list[i].elements[0].data[j][2])
			points.append(aux)
			# Así obtendremos las normales ordenadas, para poder adjuntar a la línea que necesitemos.
			auxNormals = []
			if(len(my_list[i].elements[0].data[j])==6):
				auxNormals.append(my_list[i].elements[0].data[j][3])
				auxNormals.append(my_list[i].elements[0].data[j][4])
				auxNormals.append(my_list[i].elements[0].data[j][5])
			else:
				auxNormals.append(my_list[i].elements[0].data[j][6])
				auxNormals.append(my_list[i].elements[0].data[j][7])
				auxNormals.append(my_list[i].elements[0].data[j][8])
			normals.append(auxNormals)
		newPoints.append(points)
		newNormals.append(normals)
	return newPoints, newNormals

## Esta función recoge una lista de puntos con sus coordenadas, y devuelve un índice que
## indica los kVecinos más cercanos ya ordenados.
## El formato es [nº de punto, vector más cercano, vector menos cercano, vector aún menos cercano, ....]
## Al haber escrito kNeighbors+1, nos halla el propio punto más kNeighbors. [Punto, kNeighbor1, kNeigbor2, ...]
## Esto lo hago pq sino el propio punto está incluído en el nº de kNeighbors.
## Requiere:
from sklearn.neighbors import NearestNeighbors
import numpy as np
def neighbors(my_list, kNeighbors):
	neighborsIndexList = []
	for i in range(0, len(my_list)):
		neighborsIndex = []
		neigh = NearestNeighbors(n_neighbors=kNeighbors+1)
		neigh.fit(my_list[i])
		neighborsIndex = neigh.kneighbors(my_list[i], return_distance = False)
		neighborsIndexList.append(neighborsIndex.tolist())
	return neighborsIndexList



## NEW
## Esta función ordena los puntos y las normales de tal modo que cada punto tiene X vecinos 
## en la misma fila, y se devuelve otra lista con sus normales organizadas tmb


def standarizeOutput(points, normals, index):
	predictorsList = []
	targetList = []
    # Este for se recorre si añadimos más de una nube de puntos.
	for i in range(0, len(points)):
		auxList = []
        # Este for se recorre por cada item de una nube de puntos.
		for j in range(0, len(index[i])):
			row = []
            # Este for se recorre tantas veces como vecinos haya.
			for u in range(0, len(index[i][j])):
				for z in range(0, 3):
					row.append(points[i][index[i][j][u]][z])
			targetList.append(normals[i][index[i][j][0]])
			auxList.append(row)
		predictorsList = predictorsList + auxList
		#finalList.append(auxList)
	return predictorsList, targetList



from sklearn.preprocessing import normalize
def normalizePoints(points):
    normalizedPoints = normalize(points, norm="max")
    return normalizedPoints



## Tenemos que cargar los PLY con PCToPCList(my_list, filePath), una vez tengamos
## todos los items cargados en la lista, podemos generar el input para la red neuronal.
def assembleInput(pathFile, my_list = []):
    points = []
    normals = []
    predictorsList = []
    targetList = []
    
    loadPC(my_list, pathFile)

    print("Loading figure ...")
    
    points, normals = splitPC(my_list)
    print("Splitting normals and points ...")
    index = neighbors(points, 8)
    print("Getting the neighborhood ...")
    #Si pongo esta y quito restarPunto normalizo los puntos organizados por linea de vecinos sin restarles nada.
    predictorsList, targetList = standarizeOutput(points, normals, index)
    #predictorsList, targetList = standarizeOutput(points, normals, index)
    print("Standarizing Output ..")
    #predictors = restarPunto(predictorsList)
    # predictors = restarPuntoSinPrimerPunto(predictorsList, kNeighbors)
    # Si desactivamos restarPuntoSinPrimerPunto y activamos esta, nos normaliza pero sin restar
    # el primer punto al resto de puntos.
    normalizedPredictorsList = normalizePoints(predictorsList)
    # normalizedPredictorsList = normalizePoints(predictors)
    print("Normalizing Output ..")
    return normalizedPredictorsList, targetList

