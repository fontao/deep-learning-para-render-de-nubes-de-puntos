#version 330 core
layout (location = 0) in vec3 aPosition;
layout (location = 0) in vec3 aNormal;
layout (location = 2) in vec3 aColor;

out vec3 ourColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	gl_Position = projection * view * model * vec4(aPosition, 1.0f);
    gl_PointSize = 5;
	ourColor = aColor;
}