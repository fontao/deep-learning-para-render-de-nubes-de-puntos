#version 330 core
layout (points) in;
layout (line_strip, max_vertices = 2) out;

in VS_OUT {
    vec3 normal;
    vec3 color;
} gs_in[];

out GS_OUT {
    vec3 normal;
    vec3 color;
} gs_out;

uniform float normalLineSize;


//const float MAGNITUDE = 0.1;

void main() {    
    int i;
    for (i=0;i<gl_in.length();i++)
    {
        gs_out.color = gs_in[i].color;

    
        gl_Position = gl_in[i].gl_Position; 
        EmitVertex();
        gl_Position = (gl_in[i].gl_Position + vec4(gs_in[i].normal, 0.0) * normalLineSize);
        EmitVertex();
    }
    EndPrimitive();
}  