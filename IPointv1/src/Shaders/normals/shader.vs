#version 330 core
layout (location = 0) in vec3 aPosition;
layout (location = 2) in vec3 aSecondaryNormal;
layout (location = 4) in vec3 inColorDifference;

out VS_OUT {
	vec3 normal;
	vec3 color;
} vs_out;


uniform mat4 model, view, projection;


vec4 coordPosition;

void main()
{
	//project = projection;
	coordPosition = view * model * vec4(aPosition, 1.0);
	gl_Position = projection * coordPosition;
    gl_PointSize = 1;

	mat3 normalMatrix = mat3(transpose(inverse(view * model)));
	vs_out.normal = vec3(vec4(normalMatrix * aSecondaryNormal, 0.0));
	vs_out.color = inColorDifference;

}