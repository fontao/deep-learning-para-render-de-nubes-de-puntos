#version 330 core
layout (location = 0) in vec3 aPosition;
layout (location = 0) in vec3 aNormal;
layout (location = 2) in vec3 aColor;
layout (location = 3) in float aRadius;

out vec3 ourColor;

out float oRadius;

uniform mat4 model, view, projection;
uniform float near, far, top, bottom, userRadiusFactor;
uniform int height;
uniform bool colorEnabled;

vec4 coordPosition;  // posición en coordenadas de cámara.

void main()
{
	oRadius = aRadius * userRadiusFactor;

	coordPosition = view * model * vec4(aPosition, 1.0);
	gl_Position = projection * coordPosition;
    gl_PointSize = 2 * oRadius * (near / coordPosition.z) * (height / (top-bottom));
	if (colorEnabled) ourColor = aColor;
	else ourColor = vec3(1.0f, 0.65f, 0.0f);
}