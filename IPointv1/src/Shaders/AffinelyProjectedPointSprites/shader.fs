#version 330 core
out vec4 FinalColor;

in vec3 outColor;
in vec3 outNormal;
in float outZCoordPosition;

uniform float near, far;

vec3 test;
float zBuffer;

in vec3 ourColor;

void main()
{
    //p. 278 -- ahorrar sqrt * 2, componentes.... > 1 -> discard
	test.x = gl_PointCoord.x - 0.5;
	test.y = gl_PointCoord.y - 0.5;
	test.z = -(outNormal.x/outNormal.z) * test.x - (outNormal.y/outNormal.z) * test.y;
	if (length(test) > 0.5)
		discard;

	//p. 279
	zBuffer = (outZCoordPosition + test.z * 0.1);
	gl_FragDepth = ((1.0 / zBuffer) * ( (far * near) / (far - near) ) + ( far / (far - near) ));

    FinalColor = vec4(outColor, 1.0);
}