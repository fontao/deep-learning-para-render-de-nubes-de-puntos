#version 330 core
layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inSecondaryNormal;
layout (location = 3) in vec3 inColor;
layout (location = 4) in vec3 inColorDifference;
layout (location = 5) in float inRadius;

out vec3 outColor;
out vec3 outNormal;
out float outRadius;
out float outZCoordPosition;


uniform mat4 model, view, projection;
uniform mat3 normalMatrix;
uniform float near, far, top, bottom, userRadiusFactor;
uniform int height;
uniform bool colorEnabled;

vec4 coordPosition;  // posición en coordenadas de cámara.

void main()
{
	outNormal = normalize(normalMatrix * inSecondaryNormal);

	if(abs(outNormal.z) <= 0.1)
		outNormal.z = 0.1;

	outRadius = inRadius * userRadiusFactor;

	coordPosition = view * model * vec4(inPosition, 1.0);
	gl_Position = projection * coordPosition;
    gl_PointSize = 2 * outRadius * (near / coordPosition.z) * (height / (top-bottom));
	
	//if(dot(coordPosition.xyz, outNormal) > 0)
	//	gl_Position.w = 0;

	if (colorEnabled) outColor = inColor;
	else outColor = vec3(0.91f, 0.58f, 0.51f);

	outZCoordPosition = coordPosition.z;
}