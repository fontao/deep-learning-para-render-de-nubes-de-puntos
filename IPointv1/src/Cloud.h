#pragma once

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/filter.h>
#include <iostream>

#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/common/io.h>
#include <pcl/features/normal_3d.h>

typedef pcl::PointCloud<pcl::PointXYZRGBNormal> CloudType;

// ## Methods ##
CloudType::Ptr loadPLYFile(std::string pathFile, CloudType::Ptr cloud);
float getMaxDistance(CloudType::Ptr cloud, Eigen::Vector4f centroId);
pcl::PointCloud<pcl::PointXYZ> copyCloudToCloudXYZ(CloudType::Ptr src);
pcl::PointCloud<pcl::Normal> copyCloudXYZToCloudNormal(CloudType::Ptr src);
pcl::PointCloud<pcl::Normal> copyCloudXYZNormalToCloudNormal(CloudType::Ptr src);
pcl::PointCloud<pcl::Normal>::Ptr estimateNormal(CloudType::Ptr cloud_xyzrgb);
