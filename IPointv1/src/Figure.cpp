#include "Figure.h"
#include <pcl/kdtree/kdtree_flann.h>
#include <GL/glew.h>
#include <math.h> 
#include <glm/gtx/string_cast.hpp>
#include <glm/ext.hpp>

using namespace std::chrono;

//CONSTRUCTOR ON "Figure.h"

// PRIVATE

// ## Methods ##

	vector<float> Figure::p_getRadius()
	{
		vector<float> radius;
		
		int K = 16;
		std::vector<glm::vec3> positions;
		pcl::KdTreeFLANN<pcl::PointXYZRGBNormal> kdtree;
		std::vector<int> k_indices;
		std::vector<float> k_distances;

		kdtree.setInputCloud(p_cloud);

		std::vector<int> pointIdxNKNSearch(K);
		std::vector<float> pointNKNSquaredDistance(K);

		for (unsigned int i = 0; i < p_cloud->size(); i++) {

			if ( kdtree.nearestKSearch (p_cloud->points[i], K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
				radius.push_back(sqrt(pointNKNSquaredDistance[K-1]));


		}

		return radius;
	}

	vector<float> Figure::p_normalDifference(pcl::PointCloud<pcl::Normal>::Ptr cloud)
	{	
		vector<float> diferencias;
		
		for (size_t i=0; i<p_cloud->size();i++)
		{
			diferencias.push_back(abs(cloud->points[i].normal_x - p_cloud->points[i].normal_x) +
	        					  abs(cloud->points[i].normal_y - p_cloud->points[i].normal_y) +
	        					  abs(cloud->points[i].normal_z - p_cloud->points[i].normal_z));
		}
		return diferencias;
	}


	vector<glm::vec3> Figure::p_assignColor(vector<float> diferencias)
	{
		vector<glm::vec3> colors;

		for (size_t i=0; i<diferencias.size();i++)
		{
			glm::vec3 aux;
			if (diferencias[i] <= 3)
			{
				aux =  glm::vec3(diferencias[i]/3, 1, 0);
			}else 
				aux = glm::vec3(1, 2-(diferencias[i]/3), 0);
			colors.push_back(aux);
		}

		return colors;
	}




 	vector<cloudData> Figure::p_cloudToArray()
	{
		vector<cloudData> vecCloudData;
		cloudData cloudData;

		if (p_cloud->points[0].r == 0 && 
			p_cloud->points[0].g == 0 &&
			p_cloud->points[0].b == 0) 
			{
				p_colorEnabled = false;
				std::cout << "COLOR NOT FOUND!" << std::endl;
			}


		int i = round(p_cloud->size()/4);

			if (p_cloud->points[i].normal_x == 0 || !p_cloud->points[i].normal_y || !p_cloud->points[i].normal_z) 
				std::cout << "NORMALS NOT FOUND! " << std::endl;


		for (size_t j=0; j<p_cloud->size();j++)
    	{	
			//Position
	        cloudData.position = glm::vec3(p_cloud->points[j].x, 
			                      		   p_cloud->points[j].y,
	                              		   p_cloud->points[j].z);
											 

			//Normals			
			cloudData.normal = glm::vec3(p_cloud->points[j].normal_x, 
	        							 p_cloud->points[j].normal_y, 
	        							 p_cloud->points[j].normal_z);

			cloudData.secondaryNormal = cloudData.normal;
			//RBG
			cloudData.rgb = glm::vec3(p_cloud->points[j].r/255.f, 
				    				  p_cloud->points[j].g/255.f, 
	        						  p_cloud->points[j].b/255.f);

			cloudData.colorDifference = glm::vec3(0, 
				    				  			  1, 
	        						 			  0);

			//Radius
			cloudData.radius = p_radius[j];

			
			
			vecCloudData.push_back(cloudData);
	    }

		return vecCloudData;
	}


	void Figure::init()
	{
		p_colorEnabled = true;
		p_PCLEstimatedNormalsFlag = false;
		p_myEstimatedNormalsFlag = false;
		p_userRadiusFactor = 1.0f;
		p_normalLineSize = 0.1f;
	}

// PUBLIC
// ## Methods ##
	void Figure::loadFigure(string pathFile)
	{
		CloudType::Ptr cloud (new CloudType);
		Figure::init();
		p_cloud = loadPLYFile(pathFile, cloud);
		//Aqui se controla cuando carga un archivo que no existe devuelva un mensaje, no un core dump
		if (p_cloud == NULL)
		{
			return;
		}
		p_cloud = cloud;
		p_cloudSize = cloud->size();

		p_radius = Figure::p_getRadius();

		p_vecCloudData = Figure::p_cloudToArray();

		pcl::PointCloud<pcl::Normal> normal;
		normal = copyCloudXYZNormalToCloudNormal(p_cloud);
		p_realNormals = normal.makeShared();
		vector<float> diferencias;
		diferencias = Figure::p_normalDifference(p_realNormals);
		p_realNormalsColors = Figure::p_assignColor(diferencias);
		std::cout << "Press H key to see the HELP menu ..." << std::endl;

	}

	void Figure::loadToGPU(unsigned int& VAO, unsigned int& VBO)
	{
		// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		//glBufferData(GL_ARRAY_BUFFER, 3 * this->Figure::getCloudSize() * sizeof(float), this->Figure::getCloudPositions(), GL_STATIC_DRAW);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cloudData)*Figure::getCloudSize(), &p_vecCloudData[0], GL_STATIC_DRAW);
		// position attribute
		//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(cloudData), (void*)0);
		glEnableVertexAttribArray(0);
		// normal attribute
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(cloudData), (void*)(sizeof(glm::vec3)));
		glEnableVertexAttribArray(1);
		// PCL normal attribute
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(cloudData), (void*)(2 * sizeof(glm::vec3)));
		glEnableVertexAttribArray(2);
		// color attribute
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(cloudData), (void*)(3 * sizeof(glm::vec3)));
		glEnableVertexAttribArray(3);
		// color difference attribute
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(cloudData), (void*)(4 * sizeof(glm::vec3)));
		glEnableVertexAttribArray(4);
		// radius attribute
		glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(cloudData), (void*)(5 * sizeof(glm::vec3)));
		glEnableVertexAttribArray(5);
		/*glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);*/

		// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
		// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Figure::loadNormalstoGPU(pcl::PointCloud<pcl::Normal>::Ptr cloud, vector<glm::vec3> differenceNormalsColors)
	{
		for (size_t j=0; j<p_cloud->size();j++)
		{
			//Normals			
			p_vecCloudData.at(j).secondaryNormal = glm::vec3(cloud->points[j].normal_x, 
	        							 		  			 cloud->points[j].normal_y, 
	        							         			 cloud->points[j].normal_z);
			p_vecCloudData.at(j).colorDifference = differenceNormalsColors[j];
		}

	}

	void Figure::loadPCLNormalsToGPU()
	{
		Figure::loadNormalstoGPU(p_PCLNormals, p_PCLDifferenceNormalsColors);
	}

	void Figure::loadMyNormalsToGPU()
	{
		Figure::loadNormalstoGPU(p_myEstimatedNormals, p_myDifferenceNormalsColors);
	}

	void Figure::loadRealNormalsToGPU()
	{
		
		Figure::loadNormalstoGPU(p_realNormals, p_realNormalsColors);
	}




	void Figure::calculateMetrics(vector<float> diferencias)
	{
		float sum = 0, initial = 0;
		int error10 = 0, error25 = 0, error50 = 0, error75 = 0;

		for (size_t i=0; i<=diferencias.size();i++)
		{	
			float aux = (diferencias[i]/6);
			sum = sum + aux;
			if (aux > 0.1 and aux < 0.25)
				error10 = error10 + 1;
			if (aux > 0.25 and aux < 0.50)
				error25 = error25 + 1;
			if (aux > 0.50 and aux < 0.75)
				error50 = error50 + 1;
			if (aux > 0.75)
				error75 = error75 + 1;

		}

    	//std::cout << "The sum of all floats: " << sum << std::endl;

    	float mean = sum / diferencias.size();
	    std::cout << std::endl << "The mean of all floats: " << mean << std::endl;
	    for (size_t i=0; i<=diferencias.size();i++)
		{	
			float aux = (diferencias[i]/6);
			initial = initial + pow(aux - mean, 2);
		}
		float var =  initial / diferencias.size();

		std::cout << "The varianza is: " << var << std::endl;
	    //float var = std::accumulate(diferencias.begin(), diferencias.end(), 0, Figure::normalize_values<float>{ mean }) / diferencias.size();
	    float sd = sqrt(var);
	    std::cout << "The standard deviation is: " << sd << std::endl;
	    std::cout << "Number of errors: " << std::endl;
	    std::cout << "                 10%: " << error10 << std::endl;
	    std::cout << "                 25%: " << error25 << std::endl;
	    std::cout << "                 50%: " << error50 << std::endl;
	    std::cout << "                 75%: " << error75 << std::endl;

	}

	void Figure::addPCLEstimatedNormals()
	{
		auto start = high_resolution_clock::now();
        p_PCLNormals = estimateNormal(Figure::getCloud());
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
		vector<float> diferencias;
		diferencias = Figure::p_normalDifference(p_PCLNormals);
		p_PCLDifferenceNormalsColors = Figure::p_assignColor(diferencias);
		Figure::loadNormalstoGPU(p_PCLNormals, p_PCLDifferenceNormalsColors);
		Figure::setTruePCLEstimatedNormalsFlag();
		Figure::calculateMetrics(diferencias);
		std::cout << "DONE! "<< std::endl;
		std::cout << "PCL execution time: " << duration.count() << " milliseconds" << std::endl;
	}


	void Figure::loadMyEstimatedNormals(string pathFile)
	{
		CloudType::Ptr cloud (new CloudType);

		cloud = loadPLYFile(pathFile, cloud);
		//Aqui se controla cuando carga un archivo que no existe devuelva un mensaje, no un core dump
		if (cloud == NULL)
		{
			return;
		}
		if (cloud->size() != p_cloudSize) 
		{
			std::cout << "The cloud size of the normals you are trying to load, doesn't fit the actual figure. " << std::endl 
			<< "Actual cloud size: " << p_cloudSize << ", loaded cloud size: " << cloud->size() << std::endl;
			PCL_ERROR ("Load a file with the same amount of points. \n");
			cloud = NULL;
			return;
		}	
		pcl::PointCloud<pcl::Normal> normal;
		normal = copyCloudXYZToCloudNormal(cloud);
		p_myEstimatedNormals = normal.makeShared();
		vector<float> diferencias;
		diferencias = Figure::p_normalDifference(p_myEstimatedNormals);
		p_myDifferenceNormalsColors = Figure::p_assignColor(diferencias);
		Figure::loadNormalstoGPU(p_myEstimatedNormals, p_myDifferenceNormalsColors);
		Figure::setTrueMyEstimatedNormalsFlag();
		Figure::calculateMetrics(diferencias);
		std::cout << "DONE!" << std::endl;
	}


	void Figure::moreUserRadiusFactor()
	{
		if (p_userRadiusFactor>=12.0f) p_userRadiusFactor = 12.0f;
		else this->p_userRadiusFactor = p_userRadiusFactor + 0.01f;
	}

	void Figure::lessUserRadiusFactor()
	{
		if (p_userRadiusFactor<=0.00125f) p_userRadiusFactor = 0.00125f;
		else this->p_userRadiusFactor = p_userRadiusFactor - 0.01f;
	}

	void Figure::resetFigure() 
	{
		this->p_userRadiusFactor = p_userRadiusFactorRESET;
		this->p_normalLineSize = p_normalLineSizeRESET;
	}

	void Figure::morenormalLineSize()
	{
		if (p_normalLineSize>=1.0f) p_normalLineSize = 1.0f;
		else this->p_normalLineSize = p_normalLineSize + 0.0001f;
	}
    
	void Figure::lessnormalLineSize()
	{
		if (p_normalLineSize<=0.0001f) p_normalLineSize = 0.0001f;
		else this->p_normalLineSize = p_normalLineSize - 0.0001f;
	}




	// ## Getters ##

		CloudType::Ptr Figure::getCloud()
		{
			return this->p_cloud;
		}

        unsigned int Figure::getCloudSize()
        {
        	return this->p_cloudSize;
        }

        vector<cloudData> Figure::getCloudData()
        {
        	return this->p_vecCloudData;
        }

		bool Figure::getColorEnabled()
		{
			return p_colorEnabled;
		}

		float Figure::getUserRadiusFactor()
		{
			return p_userRadiusFactor;
		}

		float Figure::getnormalLineSize()
		{
			return p_normalLineSize;
		}


		bool Figure::getPCLEstimatedNormalsFlag()
		{
			return p_PCLEstimatedNormalsFlag;
		}

		bool Figure::getMyEstimatedNormalsFlag()
		{
			return p_myEstimatedNormalsFlag;
		}


	// ## Setters ##

	void Figure::setTruePCLEstimatedNormalsFlag()
	{
		p_PCLEstimatedNormalsFlag = true;
	}

	void Figure::setTrueMyEstimatedNormalsFlag()
	{
		p_myEstimatedNormalsFlag = true;
	}