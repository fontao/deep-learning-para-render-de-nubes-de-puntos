#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN
};

// Default camera values
const float YAW         = -90.0f;
const float PITCH       =  0.0f;
const float SPEED       =  4.0f;
const float SENSITIVITY =  0.03f;
const float ZOOM        =  45.0f;

const float NEAR        =  0.1f;
const float FAR         =  100.0f;

class Camera
{
public:
    // ## Attributes ##
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // euler Angles
    float Yaw;
    float Pitch;
    // camera options
    float MovementSpeed;
    float MouseSensitivity;
    float Zoom;
    float shiftPressed;

    // ## Constructors ##
   Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), 
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, 
    float pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), 
   MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        Position = position;
        WorldUp = up;
        Yaw = yaw;
        Pitch = pitch;
        shiftPressed = 1.0;
        updateCameraVectors(); 
    }

    /*
    // constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH);
    
    // constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch);
*/
    // ## Methods ##
    void pressShift(bool pressed);

    // returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetViewMatrix();
    
    //returns the normal matrix, used in affinely-projected-point-sprites
    glm::mat3 getNormalMatrix(glm::mat4 view);

    // processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboard(Camera_Movement direction, float deltaTime);

    // processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);

    // processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void ProcessMouseScroll(float yoffset);
    

    void reset(){
        this->Position = glm::vec3(0.0f, 0.0f, 4.0f);
        this->WorldUp = glm::vec3(0.0f, 1.0f, 0.0f);
        this->Front = glm::vec3(0.0f, 0.0f, -1.0f);
        this->Yaw = YAW;
        this->Pitch = PITCH;
        this->Zoom = ZOOM;
    }

    // ## Getters ##
    float getNearFrustum()
    {
        return NEAR;
    }

    float getFarFrustum()
    {
        return FAR;
    }

    float getBottom()
    {
        return (GLfloat) tan( 0.5f * glm::radians(Zoom)) * NEAR;
    }

    float getTop()
    {
        return -getBottom();
    }

private:

    // ## Methods ##
    // calculates the front vector from the Camera's (updated) Euler Angles
    void updateCameraVectors();
};