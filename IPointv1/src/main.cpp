
#include "Shader.h"
#include "Camera.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Figure.h"
#include <vector>
#include <iostream>

using namespace std;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void processInput(GLFWwindow *window);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

// cloud data storage
/*struct CloudData {
    glm::vec3 position;
    //glm::vec3 normal;
};

std::vector<CloudData> cloudDataVector;*/

//pruebas
bool estimatedNormalsFlag=false;
bool myEstimatedNormalsFlag=false;
int switchFlag=0;
bool normalFlag = true;
bool fpsFlag = true;
bool blockMouseFlag = true;
bool mouseFlag = true;
bool stop = true;
float stopTime = 1;
GLuint VAO, VBO;
//vector<GLuint> VBO;
ofstream myfile;

Figure figure;

// settings
const unsigned int SCR_WIDTH = 1600; //1920
const unsigned int SCR_HEIGHT = 1200; //1080

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 4.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    if( !glfwInit() )
    {
        fprintf( stderr, "Error al inicializar GLFW\n" );
        return -1;
    }  
    // Con estas llamadas de aqui abajo, indicamos que versión de OpenGL vamos a aceptar, 
    // La más baja y la más alta. En este caso solo aceptamos versión 3, y dejamos claro que será
    // Una versión core, es decir, no compatible con versiones anteriores de OpenGL.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "IPoint", NULL, NULL);
    if (window == NULL)
    {
        cout << "Failed to create GLFW window" << endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

//PRUEBA
    glfwSetKeyCallback(window, key_callback);

    //Glew Init
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        cout << "glewInit failed, aborting." << endl;
        exit (1);
    }
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE); 
    glEnable(GL_POINT_SPRITE);
    glPointParameteri(GL_POINT_SPRITE_COORD_ORIGIN, GL_LOWER_LEFT);

    glEnable(GL_DEPTH_TEST); 
    // Si no pones nada, por defecto el estado es GL_LESS, lo que significa que el depth se actualiza si el nuevo valor de depth es 
    // menor al almacenado previamente.
    glDepthFunc(GL_LESS);
    // build and compile our shader program
    // ------------------------------------
    Shader ourShader("../src/Shaders/shader.vs", "../src/Shaders/shader.fs"); // you can name your shader files however you like
    Shader normalShader("../src/Shaders/normals/shader.vs", "../src/Shaders/normals/shader.fs", "../src/Shaders/normals/shader.gs");

    //Solo necesitaremos un VAO y un VBO, los inicializamos.
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);


    //Load PLY
    figure.loadFigure("../src/PCs/doll.ply");
    figure.loadToGPU(VAO, VBO);

    //

    myfile.open ("../bin/fpshistory.txt");
    
    //



    // render loop 
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;


        myfile << 1/deltaTime << ",";


        if (!fpsFlag)
            std::cout <<  "\r" << 1/deltaTime << " FPS";

        // input
        // -----
        processInput(window);

        // render
        // ------
        //Color del fondo de pantalla.
        //BLANCO 1.0f, 1.0f, 1.0f
        //AZUL OSCURO 0.07f, 0.09f, 0.27f
        //NARANJA CLARITO 0.91f, 0.58f, 0.51f
        //GRIS CLARITO 0.71f, 0.71f, 0.71f
        //AZUL CLARITO 0.64f, 0.84f, 0.92f
        glClearColor( 1.0f, 1.0f, 1.0f, 0.5f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // tell GLFW to capture our mouse
        if (!mouseFlag)
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        else 
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


        
        // activate shader
        ourShader.use();



        ourShader.setFloat("near", camera.getNearFrustum());
        ourShader.setFloat("far", camera.getFarFrustum());
        ourShader.setInt("height", SCR_HEIGHT);
        ourShader.setFloat("top", camera.getTop());
        ourShader.setFloat("bottom", camera.getBottom());
        ourShader.setBool("colorEnabled", figure.getColorEnabled());
        ourShader.setFloat("userRadiusFactor", figure.getUserRadiusFactor());


        // pass projection matrix to shader (note that in this case it could change every frame)
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, camera.getNearFrustum(), camera.getFarFrustum());
        ourShader.setMat4("projection", projection);

        // camera/view transformation
        glm::mat4 view = camera.GetViewMatrix();
        ourShader.setMat4("view", view);

        glm::mat3 normalMatrix = camera.getNormalMatrix(view);
        ourShader.setMat3("normalMatrix", normalMatrix);

        glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first

        if(!stop) 
            model = glm::rotate(model, (float)glfwGetTime() * glm::radians(50.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        else {
            model = glm::rotate(model, stopTime * glm::radians(50.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        }
        model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        ourShader.setMat4("model", model);

        glBindVertexArray(VAO);
        glDrawArrays(GL_POINTS, 0, figure.getCloudSize());
        glBindVertexArray(0);

        // then draw model with normal visualizing geometry shader
        normalShader.use();
        normalShader.setMat4("projection", projection);
        normalShader.setMat4("view", view);
        normalShader.setMat4("model", model);
        normalShader.setFloat("normalLineSize", figure.getnormalLineSize());
        if(!normalFlag) {
            glBindVertexArray(VAO);
            glDrawArrays(GL_POINTS, 0, figure.getCloudSize());
            glBindVertexArray(0);
        }
        // Sirve para controlar no poder pulsar varias veces O (Open), probé varios métodos, pero solo este parece funcionar por ahora.
        figure.mutex_open = false;

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        // Esta función comprueba si se está llevando a cabo alguna acción, como pulsar una tecla.
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);


    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
        std::cout << std::endl;
        myfile.close();
    }

    //Movimiento
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        camera.pressShift(true);
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
        camera.pressShift(false);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        camera.ProcessKeyboard(UP, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
        camera.ProcessKeyboard(DOWN, deltaTime);

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        figure.moreUserRadiusFactor();
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        figure.lessUserRadiusFactor();
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        figure.morenormalLineSize();
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        figure.lessnormalLineSize();
    
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    //(C) ROTAR
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
        if(stop) {
            stop = false;
            stopTime = (float)glfwGetTime();
        }
        else stop = true;
    }

    //(E) ESTIMAR NORMALES CON PCL
    if (key == GLFW_KEY_E && action == GLFW_PRESS)
    {
        if (!figure.getPCLEstimatedNormalsFlag())
        {
            std::cout << "Estimating normals using PCL ... " << std::endl << "This process may take a little while, please, be patient." << std::endl;
        // estimate normals
            
            figure.addPCLEstimatedNormals();
            figure.loadToGPU(VAO, VBO);
            glfwSetWindowTitle(window, "IPoint: PCL");
            
        }else 
            std::cout << "PCL estimated normals are already loaded!." << std::endl;


    }

    //(L) Cargar NORMALES ESTIMADAS POR MI
    if (key == GLFW_KEY_L && action == GLFW_PRESS)
    {
        if (!figure.getMyEstimatedNormalsFlag())
        {
            if (!figure.mutex_open)
            {
                string pathToFile;
                figure.mutex_open = true;
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                std::cout << "Loading Deep Learning estimated normals..." << std::endl;
                cout << "Open File: ";
                cin >> pathToFile;

                figure.loadMyEstimatedNormals(pathToFile);
                figure.loadToGPU(VAO, VBO);
                glfwSetWindowTitle(window, "IPoint: Deep Learning");
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            }
        }else 
            std::cout << "Deep Learning estimated normals are already loaded!." << std::endl;


    }

    //(K) CAMBIAR ENTRE LAS NORMALES DE PCL Y DEEP LEARNING
    if (key == GLFW_KEY_K && action == GLFW_PRESS)
    {
        switch (switchFlag)
        {
            case 0:
                if (figure.getMyEstimatedNormalsFlag())
                {
                    glfwSetWindowTitle(window, "IPoint: Deep Learning");
                    figure.loadMyNormalsToGPU();
                    figure.loadToGPU(VAO, VBO);
                }
                switchFlag = 1;
                break;
            case 1:
                if (figure.getPCLEstimatedNormalsFlag())
                {
                    glfwSetWindowTitle(window, "IPoint: PCL");
                    figure.loadPCLNormalsToGPU();
                    figure.loadToGPU(VAO, VBO);
                }
                switchFlag = 2;
                break;
            case 2:
                glfwSetWindowTitle(window, "IPoint");
                figure.loadRealNormalsToGPU();
                figure.loadToGPU(VAO, VBO);
                switchFlag = 0;
                break;
        }


    }
    
    //(H) HEEEEEELLLLPPPP
    if (key == GLFW_KEY_H && action == GLFW_PRESS) 
    {
        std::cout << "HELP IS COMING!" << std::endl
        << "--Esc--Press it to close the program--" << std::endl
        << "--B--Enable/Disable mouse--" << std::endl
        << "--W, A, S, D, Ctrl, Space and Mouse--Movement--" << std::endl
        << "--Shitf (pressed)--Speed up movement--" << std::endl
        << "--R--Reset the position of the figure--" << std::endl
        << "--O--Load a new PLY file--" << std::endl 
        << "--C--Auto-Rotation Start/Stop--" << std::endl
        << "--Up/Down arrows--Higher/lower splat size--" << std::endl
        << "--N--Show/hide normals--" << std::endl
        << "--Right/Left arrows--Higher/lower point normal size--" << std::endl
        << "--E--Estimate normals with PCL--" << std::endl
        << "--L--Load Deep Learning estimated normals--" << std::endl
        << "--K--Switch between loaded normals (Real,PCL,DeepLearning)--" << std::endl
        //<< "--F--Show/hide Frames Per Second--" << std::endl
        << "--X--Lock/unlock mouse movement--" << std::endl;


    }

    //(B) HABILITAR Y DESHABILITAR EL RATÓN
    if (key == GLFW_KEY_B && action == GLFW_PRESS) 
    {
        if (mouseFlag)
            mouseFlag = false;
        else 
            mouseFlag = true;
    }

    //(N) MOSTRAR NORMALES
    if (key == GLFW_KEY_N && action == GLFW_PRESS) 
    {
        if (normalFlag)
            normalFlag = false;
        else 
            normalFlag = true;
    }
    
    //(R) RESETEAR LA FIGURA
    if (key == GLFW_KEY_R && action == GLFW_PRESS) 
    {
        figure.resetFigure();
        camera.reset();
    }

    //(O) CARGAR UNA NUEVA FIGURA
    if (key == GLFW_KEY_O && action == GLFW_PRESS)
    {
        string pathToFile;

        if (!figure.mutex_open)
        {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfwSetWindowTitle(window, "IPoint");
            figure.mutex_open = true;
            cout << "Open File: ";
            cin >> pathToFile;
            //std::cin.get();

            
            figure.loadFigure(pathToFile);
            figure.loadToGPU(VAO, VBO);
            
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }

    //(F) Print frames per second
    /*if (key == GLFW_KEY_F && action == GLFW_PRESS) 
    {
        if (fpsFlag)
            fpsFlag = false;
        else 
            fpsFlag = true;
    }*/

    //(X) Print frames per second
    if (key == GLFW_KEY_X && action == GLFW_PRESS) 
    {
        if (blockMouseFlag)
        {
            blockMouseFlag = false;
            glfwSetCursorPosCallback(window, NULL);
        }
        else
        { 
            blockMouseFlag = true;
            glfwSetCursorPosCallback(window, mouse_callback);
        }

    }

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}
