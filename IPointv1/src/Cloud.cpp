#include "Cloud.h"
#include <iostream>
#include <pcl/common/common.h>

using namespace std;

typedef pcl::PointCloud<pcl::PointXYZRGBNormal> CloudType;

pcl::PointCloud<pcl::PointXYZ> copyCloudToCloudXYZ(CloudType::Ptr src)
{
    pcl::PointCloud<pcl::PointXYZ> dst;

    //std::cout << "COSAS: " << src->width << std::endl << src->height << std::endl << src->is_dense << std::endl << src->size() << std::endl;
    dst.width = src->width;
    dst.height = src->height;
    dst.is_dense = true;
    dst.points.resize(dst.width * dst.height);
    for (unsigned int i = 0; i < src->points.size(); i++)
    {
        dst.points[i].x = src->points[i].x;
        dst.points[i].y = src->points[i].y;
        dst.points[i].z = src->points[i].z;
    }
    return dst;
}

pcl::PointCloud<pcl::Normal> copyCloudXYZToCloudNormal(CloudType::Ptr src)
{
    pcl::PointCloud<pcl::Normal> dst;

    dst.width = src->width;
    dst.height = src->height;
    dst.is_dense = true;
    dst.points.resize(dst.width * dst.height);
    for (unsigned int i = 0; i < src->points.size(); i++)
    {
        dst.points[i].normal_x = src->points[i].x;
        dst.points[i].normal_y = src->points[i].y;
        dst.points[i].normal_z = src->points[i].z;
    }
    return dst;
}

pcl::PointCloud<pcl::Normal> copyCloudXYZNormalToCloudNormal(CloudType::Ptr src)
{
    pcl::PointCloud<pcl::Normal> dst;
    
    dst.width = src->width;
    dst.height = src->height;
    dst.is_dense = true;
    dst.points.resize(dst.width * dst.height);
    for (unsigned int i = 0; i < src->points.size(); i++)
    {
        dst.points[i].normal_x = src->points[i].normal_x;
        dst.points[i].normal_y = src->points[i].normal_y;
        dst.points[i].normal_z = src->points[i].normal_z;
    }
    return dst;
}

CloudType::Ptr loadPLYFile(std::string pathFile, CloudType::Ptr cloud)
{
    std::string loaded = "";


    if (pcl::io::loadPLYFile (pathFile, *cloud) == -1) //* load the file
    {
        loaded="PLY File load ERROR";
        PCL_ERROR ("Couldn't read PLY file. \n");
        return NULL;
        std::cout << "STEP 1" << std::endl;
        
    }else loaded="PLY File load SUCCESS";


    std::cout << loaded << std::endl;
    std::cout << std::endl << "Cloud size: " << cloud->size() << std::endl;
    cout << endl << "Removing NaN Points ..." << endl;
    std::vector<int> indices;
    int pointsBefore = cloud->size();
    pcl::removeNaNFromPointCloud(*cloud, *cloud, indices);

    if (pointsBefore - cloud->size() > 0)
        cout << "-> Deleted " << (pointsBefore - cloud->size()) << " NaN Points." << endl;
    else std::cout << "No NaN Points FOUND" << std::endl;

    

    if(cloud->is_dense)
    {
        //Move to origin
        CloudType::Ptr centeredCloud (new CloudType);
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid (*cloud, centroid);
        pcl::demeanPointCloud<pcl::PointXYZRGBNormal> (*cloud, centroid, *cloud);

        float maxDistance = getMaxDistance(cloud, centroid);

        // Al dividir entre la distancia máxima, conseguimos normalizar la entrada de posición
        // de puntos entre -1 y 1.
        for (size_t i = 0; i < cloud->points.size (); ++i) {
            cloud->points[i].x = cloud->points[i].x/maxDistance;
            cloud->points[i].y = cloud->points[i].y/maxDistance;
            cloud->points[i].z = cloud->points[i].z/maxDistance;
        }

        return cloud;

    }else
    {
        PCL_ERROR( "The PointCloud is not dense" );
    }

    return NULL;

}

float getMaxDistance(CloudType::Ptr cloud, Eigen::Vector4f centroId)
{
    //Haciendo comprobaciones, esta función no es 100% precisa, pero considero que es lo suficientemente precisa.
    //Hayamos valores superiores en determinados casos, pero muy cercanos a 1.

    //Get MaxDistance for Scaling
    Eigen::Vector4f farestPoint;
    pcl::getMaxDistance(*cloud, centroId, farestPoint);
    float maxDistance = max( max( abs(farestPoint.x()), abs(farestPoint.y()) ) , abs(farestPoint.z()) );
    std::cout << std::endl;
    cout << "Scaling Figure ..." << endl;
    cout << "Max. Distance: " << maxDistance << endl;
    std::cout << std::endl;


    return maxDistance;
}


pcl::PointCloud<pcl::Normal>::Ptr estimateNormal(CloudType::Ptr cloud_xyzrgb)
{
    pcl::NormalEstimation<pcl::PointXYZRGBNormal, pcl::Normal> ne;
    ne.setInputCloud (cloud_xyzrgb);

    pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBNormal> ());
    ne.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch (0.03);

    // Compute the features
    ne.compute (*cloud_normals);

    return cloud_normals;
}
