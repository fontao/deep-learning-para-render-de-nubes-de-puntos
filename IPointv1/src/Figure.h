#pragma once

#include "Cloud.h"
#include <vector>
#include <glm/glm.hpp>
#include <pcl/common/common.h>

using namespace std;

// Este struct contiene los datos de posición, normal y rgb de los POINT CLOUD
// en forma de array, un datos tras otro.
struct cloudData {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 secondaryNormal;
    glm::vec3 rgb;
    glm::vec3 colorDifference;
    float radius;
};


struct auxXYZ {
    glm::vec3 aux;
};

class Figure
{
public:
    //Variables
    bool mutex_open;

    
    //Methods
    void loadFigure(string pathFile);
    void loadToGPU(unsigned int& VAO, unsigned int& VBO);
    void loadNormalstoGPU(pcl::PointCloud<pcl::Normal>::Ptr cloud, vector<glm::vec3> differenceNormalsColors);
    void loadMyEstimatedNormals(string pathFile);
    void addPCLEstimatedNormals();
    void loadPCLNormalsToGPU();
    void loadMyNormalsToGPU();
    void loadRealNormalsToGPU();
    void moreUserRadiusFactor();
    void lessUserRadiusFactor();
    void morenormalLineSize();
    void lessnormalLineSize();
    void resetFigure();
    void calculateMetrics(vector<float> diferencias);

    
    
        //Getters
    unsigned int getCloudSize();
    vector<cloudData> getCloudData();
    CloudType::Ptr getCloud();
    bool getColorEnabled();
    float getUserRadiusFactor();
    float getnormalLineSize();
    bool getPCLEstimatedNormalsFlag();
    bool getMyEstimatedNormalsFlag();
  

    //Setters
    void setTruePCLEstimatedNormalsFlag();
    void setTrueMyEstimatedNormalsFlag();

    //Constructors
    Figure()
    {
        mutex_open = false;
    }

private:
    //Variables
    CloudType::Ptr p_cloud;
    unsigned int p_cloudSize;
    vector<float> p_radius;
    bool p_colorEnabled;
    float p_userRadiusFactor;
    float p_normalLineSize;
    vector<cloudData> p_vecCloudData;
    const float p_userRadiusFactorRESET = 1.0f;
    const float p_normalLineSizeRESET = 0.1f;
    bool p_PCLEstimatedNormalsFlag;
    bool p_myEstimatedNormalsFlag;
    pcl::PointCloud<pcl::Normal>::Ptr p_PCLNormals;
    pcl::PointCloud<pcl::Normal>::Ptr p_myEstimatedNormals;
    pcl::PointCloud<pcl::Normal>::Ptr p_realNormals;
    vector<glm::vec3> p_PCLDifferenceNormalsColors;
    vector<glm::vec3> p_myDifferenceNormalsColors;
    vector<glm::vec3> p_realNormalsColors;
    
    

    
    //Methods
    void init();
    vector<cloudData> p_cloudToArray();
    vector<float> p_getRadius();
    vector<float> p_normalDifference(pcl::PointCloud<pcl::Normal>::Ptr cloud);
    vector<glm::vec3> p_assignColor(vector<float> diferencias);
    

};
